﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFishApp
{
    class Fishes
    {
        //ominaisuudet

        //toiminnot
        
        public static void AddNewFish() {
            Fish fish = new Fish();
            Console.WriteLine("Enter fisher:");
            fish.Fisher = Console.ReadLine();
            Console.WriteLine("Enter specie:");
            fish.Specie = Console.ReadLine();
            Console.WriteLine("Enter lenght:");
            fish.Lenght = CheckInput();
            Console.WriteLine("Enter weight:");
            fish.Weight = CheckInput();
            Console.WriteLine("Enter location:");
            fish.Location = Console.ReadLine();
            Console.WriteLine("Enter place:");
            fish.Place = Console.ReadLine();
            //Console.WriteLine("\n\nNew fish added: specie: {0}\nLength: {1}\nWeight: {2}\nLocation: {3}\nPlace: {4}", fish.Specie, fish.Lenght, fish.Weight, fish.Location, fish.Place);
            fish.AddFishToList(fish);
        }
        public static double CheckInput() { 
        while (true)
            {
                string input = Console.ReadLine();
                try
                {
                    double CorrectInput = Int32.Parse(input);
                    if (CorrectInput > 0) return CorrectInput;
                    else
                    {
                        Console.WriteLine($"Invalid input: \"{input}\"");
                        Console.WriteLine("Please try again");
                    }
}
                catch (FormatException)
                {
                    Console.WriteLine($"Invalid input: \"{input}\"");
                    Console.WriteLine("Please try again");

                }
            }
        }
    }
}
