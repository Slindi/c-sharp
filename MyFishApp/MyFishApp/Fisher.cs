﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MyFishApp
{
    [Serializable]
    class Fisher
    {
        //ominaisuudet
        public string Name { get; set; }
        public string PhoneNro { get; set; }
        public static List<Fisher> fishers = new List<Fisher>();

        public void AddFisherToList(Fisher f)
        {
            fishers.Add(f);
            Console.WriteLine("\nA new fisherman added to Fish-register:\n- Fisherman: {0} Phone: {1}\n", f.Name, f.PhoneNro);
        }

        public static void PrintFishers()
        {
            if (fishers.Capacity == 0) {
                Console.WriteLine("\nError! No fishers in the registry!");
            }
            else {
                Console.WriteLine("\nFishers in the registry:\n");
            foreach (Fisher f in fishers)
                {
                    Console.WriteLine(f.Name + " " + f.PhoneNro);
                }
            }
        }

        public static void PrintEverything()
        {
            if (fishers.Capacity == 0) {
                Console.WriteLine("\nError!\nNo fishers in the registry!\n");
            }
            else { 
                Console.WriteLine("\nAll the fish in the register:");
                foreach (Fisher f in fishers)
                {
                
                    Console.WriteLine("Fisherman {0} has got the following fish:\n", f.Name);
                    Fish.PrintFishes(f);
                
                }
            }
            //Fish.PrintFishes();
        }
    }
}
