﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFishApp
{
     class Fishers
    {
        
        public static void AddNewFisher() { 
            Fisher fisher = new Fisher();
            Console.WriteLine("Enter a name for the fisher:");
            fisher.Name = Console.ReadLine();
            Console.WriteLine("Enter a phone number for the fisher:");
            fisher.PhoneNro = Console.ReadLine();
            fisher.AddFisherToList(fisher);
        }
    }
}
