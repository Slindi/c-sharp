﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFishApp
{
    class Fish
    {
        //ominaisuudet
        public string Specie { get; set; }
        public double Lenght { get; set; }
        public double Weight { get; set; }
        public string Place { get; set; }
        public string Location { get; set; }
        public string Fisher { get; set; }

        public static List<Fish> fishes = new List<Fish>();

        public void AddFishToList(Fish f)
        {
            fishes.Add(f);
            Console.WriteLine("\nFisher: {0} got a new fish\n- specie: {1} {2} cm {3} kg\n- place: {4}\n- location: {5}\n", f.Fisher, f.Specie, f.Lenght, f.Weight, f.Place, f.Location);
        }
        public static void PrintFishes(Fisher fisher)
        {
            if (fishes.Capacity == 0)
            {
                Console.WriteLine("Error!\nNo fish in the registry!\n");
            }
            else { 
                foreach (Fish f in fishes)
                {
                    if (fisher.Name == f.Fisher) { 
                        Console.WriteLine("\n- specie: {0} {1} cm {2} kg\n- place: {3}\n- location: {4}", f.Specie, f.Lenght, f.Weight, f.Place, f.Location);
                    }
                }
            }
        }
        //debuggausta varten tämä metodi
        public static void PrintFishes()
        {
            foreach (Fish f in fishes)
            {
                //if (fisher.Name == f.Fisher) { 
                Console.WriteLine("\n- specie: {0} {1} {2}\n- place: {3}\n- location: {4}", f.Specie, f.Lenght, f.Weight, f.Place, f.Location);
                //}
            }
        }

        //järjestetään suurimman kalan mukaan
        public static void SortByBiggest()
        {
            fishes.Sort((x, y) => y.Weight.CompareTo(x.Weight));
            Console.WriteLine("Sorted register\n\n*** All fish in the Fish-register: ***\n");           
            foreach (Fish f in fishes)
            {
                Console.WriteLine("- {0} {1} cm {2} kg\n- place: {3}\n- location: {4}\n- Fisherman: {5}\n", f.Specie, f.Lenght, f.Weight, f.Place, f.Location, f.Fisher);
            }
        }
    }
}
