﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MyFishApp
{
    public class Menu
    {
        

        public static void PrintMainMenu()
        {
            Console.WriteLine("***************************************");
            Console.WriteLine("Welcome to MyFishApp!\n\n");
            Console.WriteLine("Available actions (choose with 1-5):");
            Console.WriteLine("1. Add new fisher to registry");
            Console.WriteLine("2. Add new fish to registry");
            Console.WriteLine("3. Print the fishers in the registry");
            Console.WriteLine("4. Print fishes in the registry");
            Console.WriteLine("5. Print fishes in the registry (biggest first)");
            Console.WriteLine("6. Quit MyFishApp");
            Console.WriteLine("****************************************\n");
            Console.WriteLine("Action:");
            ChooseAction();
            
        }
         public static void ChooseAction()
        {
            int Action;
            while (true)
            {
                string Input = Console.ReadLine();
                try
                {
                    Action = Int32.Parse(Input);
                    if (Action > 0 && Action < 7)
                        break;
                    else
                    {
                        Console.WriteLine($"Invalid input: \"{Input}\"");
                        Console.WriteLine("Please try again");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine($"Invalid input: \"{Input}\"");
                    Console.WriteLine("Please try again");
                }
            }
            DoAction(Action);
        }
        public static void DoAction(int Action) {
            switch (Action)
            {
                case 1:
                    Fishers.AddNewFisher();
                    PrintMainMenu();
                    break;
                case 2:
                    Fishes.AddNewFish();
                    PrintMainMenu();
                    break;
                case 3:
                    Fisher.PrintFishers();
                    PrintMainMenu();
                    break;
                case 4:
                    Fisher.PrintEverything();
                    PrintMainMenu();
                    break;
                case 5:
                    Fish.SortByBiggest();
                    PrintMainMenu();
                    break;
                case 6:
                    Console.WriteLine("Thank you for using MyFishApp!");
                    break;
            }
        }
        
    }
}
