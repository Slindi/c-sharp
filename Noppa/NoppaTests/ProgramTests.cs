﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Noppa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noppa.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void MainTest()
        {
            // arrange
            Noppa Noppa1 = new Noppa();

            // act
            int heitto = Noppa1.Heita();

            // assert
            Assert.IsTrue(heitto < 7 && heitto > 0);
        }
    }
}

namespace NoppaTests
{
    class ProgramTests
    {
    }
}
