﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noppa
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Noppa Noppa1 = new Noppa();
            Console.WriteLine("Die, one test throw value is {0}", Noppa1.Heita());
            Console.WriteLine("How many times would you like to throw the die");
            int[] tulokset = new int[6] { 0, 0, 0, 0, 0, 0 }; // tehdään taulukko silmälukujen määriä varten
            int tulos;
            while (true)
            {
                string syote = Console.ReadLine();
                try
                {
                    tulos = Int32.Parse(syote);
                    // Console.WriteLine(tulos);
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine($"Virheellinen syöte: '{syote}'");
                    Console.WriteLine("Anna uusi syöte");

                }

            }
            int summa = 0;
            int i = 0;
            while (i < tulos)
            {
                int heitto = Noppa1.Heita();
                tulokset[heitto - 1]++;
                i++;
            }
            int j = 1;
            foreach (int number in tulokset)
            {
                Console.WriteLine(j.ToString() + " count is {0}", number);
                summa += j * number;
                j++;
            }
            float average = (float)summa / (float)tulos;
            Console.WriteLine("Average is {0}", average);
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
        }
    }
}
