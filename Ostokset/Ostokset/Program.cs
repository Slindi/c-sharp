﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostokset
{
    class Program
    {
        static void Main(string[] args)
        {
            Invoice invoice1 = new Invoice
            {
                Customer = "Kirsi Kernel"
            };

            
            invoice1.AddItem(new InvoiceItem { Name = "milk", Price = 1.5, Quantity = 2 });
            invoice1.AddItem(new InvoiceItem { Name = "bread", Price = 2, Quantity = 3 });
            invoice1.AddItem(new InvoiceItem { Name = "sausage", Price = 3, Quantity = 1 });
            invoice1.AddItem(new InvoiceItem { Name = "beer", Price = 0.89, Quantity = 24 });
            invoice1.PrintInvoice();
        }

    }
}
