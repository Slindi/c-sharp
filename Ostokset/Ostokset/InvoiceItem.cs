﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostokset
{
    public class InvoiceItem
    {
        //ominaisuudet
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double Total {
            get {
                return Price * Quantity;
            }
            set { }

            }
        //toiminnot
        public override string ToString()
        {            
            return Name + " " + Price + "e " + Quantity + " pieces";
        }

       
    }
}
