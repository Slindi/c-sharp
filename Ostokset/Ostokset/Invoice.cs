﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostokset
{
    public class Invoice
    {
        // ominaisuudet
        public string Customer { get; set; }
        List<InvoiceItem> invoice = new List<InvoiceItem>();
        public string Total { get; set; }
        public double temp = 0;

        //toiminnot

        public void AddItem(InvoiceItem item)
        {
            invoice.Add(item);
        }

        public void PrintInvoice()
        {
            Console.WriteLine("=================================");
            Console.WriteLine("Cuntomer {0}'s invoice:", Customer);
            foreach(InvoiceItem i in invoice)
            {
                Console.WriteLine(i.ToString() + " " + i.Total + "e total");               
            }
            Console.WriteLine("=================================");
            Console.WriteLine("Total: {0} euros", GetInvoiceTotal());
        }

        public double GetInvoiceTotal()
        {
            foreach (InvoiceItem i in invoice) {
                temp += i.Total;
            }
            return temp;
        }

    }
}
