﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ostokset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ostokset.Tests
{
    [TestClass()]
    public class InvoiceTests
    {
        [TestMethod()]
        public void GetInvoiceTotalTest()
        {
            //Arrange
            Invoice invoice = new Invoice();
            invoice.AddItem(new InvoiceItem { Name = "Milk", Price = 2, Quantity = 2 });
            invoice.AddItem(new InvoiceItem { Name = "Bread", Price = 3, Quantity = 3 });
            double expected = 13;

            //Act
            double actual = invoice.GetInvoiceTotal();

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}