﻿/// @author Sami Lindgren
/// @version 15.11.2019
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linkkitorni
{
    class Program
    {
        static void Main(string[] args)
        {
            // lisätään linkkiasemat taulukkoon ja lasketaan montako riviä eli asemaa taulukossa on
            int[,] Stations = new int[3, 3]
            {
                {0, 0, 10 },
                {20, 20, 5 },
                {10, 0, 12 }
            };
            int NumberOfStations = Stations.Length / 3;

            // lisätään vastaanottimet taulukkoon ja lasketaan montako riviä eli vastaanotinta on
            int[,] Points = new int[4, 2]
            {
                {0, 0 },
                {100, 100 },
                {15, 10 },
                {18, 18 }
            };
            int NumberOfPoints = Points.Length / 2;

            string BestTower = "";
            double Power;
            int i, j;
            
            // käydään läpi vastaanottimet yksi kerrallaan
            for (j = 0; j < NumberOfPoints; j++) {
                double BestPower = 0;
                // käydään läpi linkkiasemat
                for (i = 0; i < NumberOfStations; i++)
                {
                    // lasketaan vastaanottimen etäisyys kustakin asemasta
                    double Dist = CalcDistance(Stations[i, 0], Stations[i, 1], Points[j, 0], Points[j, 1]);
                    // lasketaan signaalin vahvuus kustakin asemasta
                    Power = CalcPower(Dist, Stations[i, 2]);
                    // jos signaali on vahvempi kuin siihen asti vahvin (aloitusvahvuus 0), lisätään signaalin vahvuus vahvimmaksi signaaliksi
                    if (Power > BestPower)
                    {
                        BestPower = Power;
                        // lisätään myös lähettimen koordinaatit parhaaksi lähettimeksi
                        BestTower = Stations[i, 0] + "," + Stations[i, 1];
                    }
                }
                // jos vahvimman signaalin vahvuus on suurempi kuin nolla, tulostetaan vastaanottimen ja lähettimen paikka sekä signaalin vahvuus
                if (BestPower != 0) {
                    Console.WriteLine("Best link station for point {0},{1} is {2} with power {3}", Points[j, 0], Points[j, 1], BestTower, BestPower);
                }
                // jos signaalin vahvuus on edelleen 0, ei mikään lähetin ole tarpeeksi lähellä vastaanotinta
                else if (BestPower == 0)
                {
                    Console.WriteLine("No link station within reach for point {0},{1}", Points[j, 0], Points[j, 1]);
                }
            }
        }

        static double CalcDistance(int x, int y, int a, int b)
        {
            
            return Math.Sqrt(Math.Pow((x - a), 2) + Math.Pow((y - b), 2));
        }

        static double CalcPower(double dist, double reach)
        {
            double Power = Math.Pow((reach - dist), 2);
            if (reach > dist) { 
                return Power;
            }
            else
            {
                return 0;
            }
        }
    }
}
