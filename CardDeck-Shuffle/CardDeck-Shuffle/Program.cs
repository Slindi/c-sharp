﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T20_Sekoittaminen
{
    class Program
    {
        static void Main(string[] args)
        {
            // luodaan uusi korttipakka
            Korttipakka Pakka1 = new Korttipakka();
            // luodaan taulukko Maa, johon sijoitetaan neljä eri maata tekstinä
            string[] Maa = new string[4] { "Hertta", "Pata", "Ruutu", "Risti" };
            int i = 0;
            // käydään taulukko läpi jokaisen maan kohdalta, ja suoritetaan for-silmukka joka luo kortin annetulla maalla ja lisää arvoksi 1-13
            while (i < Maa.Length)
            {
                for (int j = 1; j <= 13; j++)
                {
                    Pakka1.LisaaKortti(new Kortti { Arvo = j, Maa = Maa[i] });
                }
                i++;
            }
            // tulostetaan kortit
            //Pakka1.TulostaKortit();
            Pakka1.Shuffle();
        }

    }
}

