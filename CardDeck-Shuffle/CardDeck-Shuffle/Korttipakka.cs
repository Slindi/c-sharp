﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T20_Sekoittaminen
{
    class Korttipakka
    {
        // ominaisuudet
        // tehdään korteille lista
        List<Kortti> Kortit = new List<Kortti>();

        // toiminnot
        // lisätään kortti korttipakkaan
        public void LisaaKortti(Kortti k)
        {
            Kortit.Add(k);
            // debuggausta varten tulostettiin kortti lisättäessä
            //Console.WriteLine(k.Maa + " " + k.Arvo);
        }
        // tulostetaan kortit käymällä lista läpi foreach-silmukalla
        public void TulostaKortit()
        {
            int i = 1;
            foreach (Kortti k in Kortit)
            {
                Console.WriteLine(i + " kortti on " + k.Maa + " #" + k.Arvo);
                i++;
            }
        }
        // korttien sekoitus
        public void Shuffle()
        {
            // tehdään uusi lista nimeltä SekoitettuPakka
            List<Kortti> SekoitettuPakka = new List<Kortti>();
            /* for-silmukka: tehdää 52 kertaa seuraava
             * valitaan satunnainen indeksi väliltä 0-korttien lukumäärä (alaraja 0 on inklusiivinen, yläraja korttien määrä eksklusiivinen) listasta Kortit
             * sijoitetaan kortti-olio edellä valitusta indeksistä apumuuttujaan apu
             * lisätään tämä olio uuteen listaan nimeltä SekoitettuPakka
             * tulostetaan kortin järjestysluku ja maa ja arvo
             * poistetaan indeksi alkuperäisestä listasta
             */
            Random rnd = new Random();
            for (int i = 1; i <= 52; i++)
            {
                int tmp = rnd.Next(0, Kortit.Count);
                Kortti apu = Kortit[tmp];
                SekoitettuPakka.Add(apu);
                // debuggauksessa käytetty apufunktio Console.WriteLine(Kortit.Count + " " + tmp + "" + );
                Console.WriteLine(i + ". kortti on " + apu.Maa + " #" + apu.Arvo);
                Kortit.RemoveAt(tmp);
            }

        }

    }
}
