﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kuviot
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape Circle1 = new Circle() { Name = "Circle", Radius = 1 };
            Shapes.AddToList(Circle1);
            Shape Circle2 = new Circle() { Name = "Circle", Radius = 2 };
            Shapes.AddToList(Circle2);
            Shape Circle3 = new Circle() { Name = "Circle", Radius = 3 };
            Shapes.AddToList(Circle3);
            Shape Rec1 = new Rectangle() { Name = "Rectangle", Height = 20, Width = 10 };
            Shapes.AddToList(Rec1);
            Shape Rec2 = new Rectangle() { Name = "Rectangle", Height = 30, Width = 20 };
            Shapes.AddToList(Rec2);
            Shape Rec3 = new Rectangle() { Name = "Rectangle", Height = 50, Width = 40 };
            Shapes.AddToList(Rec3);
            Shapes.PrintAll();
        }
    }
}
