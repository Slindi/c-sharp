﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kuviot
{
    class Rectangle : Shape
    {
        //ominaisuudet
        public double Width { get; set; }
        public double Height { get; set; }
        //toiminnot
        public override double Circumference()
        {
            return 2 * Height + 2 * Width;
        }
        public override double Area()
        {
            return Width * Height;
        }
        
        public string PrintInfo()
        {
            return "Width=" + Width + " Height=" + Height;
        }
    }
}
