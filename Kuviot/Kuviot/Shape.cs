﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kuviot
{
    public abstract class Shape
    {
        //ominaisuudet
        public string Name;

        //toiminnot
        public abstract double Area();
        public abstract double Circumference();
    }
}
