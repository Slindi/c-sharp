﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kuviot
{
    class Circle : Shape
    {
        //ominaisuudet
        public double Radius { get; set; }
        //toiminnot
        public override double Circumference()
        {
            return 2 * Math.PI * Radius;
        }
        public override double Area()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
