﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kuviot
{
    class Shapes
    {
        static List<Shape> shapes = new List<Shape>();

        public static void AddToList(Shape s)
        {
            shapes.Add(s);
        }

        public static void PrintAll()
        {
            foreach (Shape s in shapes)
            {
                if (s is Circle c) { 
                    Console.WriteLine("{0} Radius={1} Area={2} Circumference={3}", s.Name, c.Radius, s.Area(), s.Circumference());
                }
                else if (s is Rectangle r) {
                        Console.WriteLine("{0} Height={1} Width={2} Area={3} Circumference={4}", s.Name, r.Height, r.Width, s.Area(), s.Circumference());
                }
                else
                {
                    Console.WriteLine("Unknown shape!");
                }
            }
        }
    }
}
