﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Laskutoimituksia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskutoimituksia.Tests
{
    [TestClass()]
    public class ArrayCalcsTests
    {
        [TestMethod()]
        public void SumTest()
        {
            // arrange
            ArrayCalcs calc = new ArrayCalcs();
            double[] array = new double[] { 5, 7 };
            double expected = 12;

            // act
            double actual = ArrayCalcs.Sum(array);

            // assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void AverageTest()
        {
            // arrange
            ArrayCalcs calc = new ArrayCalcs();
            double[] array = new double[] { 5, 7 };
            double expected = 6;

            // act
            double actual = ArrayCalcs.Average(array);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MinTest()
        {
            // arrange
            ArrayCalcs calc = new ArrayCalcs();
            double[] array = new double[] { 10, 5, 15 };
            double expected = 5;

            // act
            double actual = ArrayCalcs.Min(array);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MaxTest()
        {
            // arrange
            ArrayCalcs calc = new ArrayCalcs();
            double[] array = new double[] { 10, 5, 15 };
            double expected = 5;

            // act
            double actual = ArrayCalcs.Min(array);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}