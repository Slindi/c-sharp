﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskutoimituksia
{
    public class ArrayCalcs
    {
        // toiminnot

        // laskee yhteen taulukon alkoiden summan ja palauttaa sen
        public static double Sum(Array a)
        {
            double summa = 0;
            foreach(double num in a)
            {
                //Console.WriteLine(num);
                summa += num;
            }
            return summa;
        }

        // laskee taulukon alkoiden summan ja palauttaa sen
        public static double Average(Array a)
        {
            double average, summa = 0;

            foreach(double num in a)
            {
                summa += num;
            }
            average = summa / a.Length;
            return average;
        }

        // palauttaa arrayn pienimmän luvun
        public static double Min(Array a)
        {
            double min = 1000; // valitaan ensimmäiseen vertailuun taulukon lukuja oletettavasti suurempi luku
            foreach (double num in a) // käydään taulukon luvut läpi yksi kerrallaan
            {
                // jos taulukon luku on pienempi kuin siihen asti pienin luku, asetetaan pienimmäksi kyseinen luku
                if (num < min)
                {
                    min = num;
                }
            }
            return min;
        }

        // palauttaa arrayn suurimman luvun

        public static double Max(Array a)
        {
            double max = -1000; // valitaan ensimmäiseen vertailuun taulukon lukuja oletettavasti pienempi luku
            foreach (double num in a) // käydään läpi taulukon luvut yksi kerrallaan
            {
                // jos taulukon luku on suurempi kuin siihen asti suurin luku, asetetaan suurimmaksi kyseinen luku
                if (num > max)
                {
                    max = num;
                }
            }
            return max;
        }
    }
}
