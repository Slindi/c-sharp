﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskutoimituksia
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 1.0, 2.0, 3.3, 5.5, 6.3, -4.5, 12.0 };
            double[] a = new double[] { };
            Console.WriteLine("Sum = {0}", ArrayCalcs.Sum(array)); // kutsutaan Sum-metodia, joka laskee yhteen taulukon luvut ja tulostetaan ruudulle summa
            Console.WriteLine("Ave = {0}", ArrayCalcs.Average(array)); // kutsutaan Average-metodia, joka laskaa taulukon lukujen keskiarvon
            Console.WriteLine("Min = {0}", ArrayCalcs.Min(array)); // kutsutaan Min-metodia, joka palauttaa taulukon pienimmän luvun
            Console.WriteLine("Max = {0}", ArrayCalcs.Max(array)); // kutusutaan Max-metodia, joka palauttaa taulukon suurimman luvun
        }
    }
}
