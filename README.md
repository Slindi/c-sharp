# C-sharp harjoitustehtävien ratkaisuja / C-sharp programming exercises I've done

## CardDeck-Shuffle

Luo korttipakan, sekoittaa kortit ja tulostaa pakan järjestyksen / Creates a card deck, shuffles it and prints the deck

## Noppa

Heittää ensin noppaa kerran ja tulostaa tuloksen. Sen jälkee kysyy käyttäjältä heittojen lukumäärän, tulostaa ruudulle heittojen keskiarvon sekä kuinka monta kertaa kukin silmäluku esiintyi. Sisältää yksikkötestin jossa testaa nopan heiton olevan välillä 1-6. / Throws a die once at first and prints the result. Then asks the user the amount of throws, prints the average and how many times each number occurred. Includes a unit test that tests that the die roll is between 1 and 6.

## Laskutoimituksia

ArrayCalcs-luokan metodit selvittävät syötetystä taulukosta lukujen summan, keskiarvon sekä pienimmän ja suurimman luvun ja tulostaa ne näytölle. Metodien toiminta varmistetaan yksikkötesteillä. / ArrayCalcs-class has methods that find the sum, average and the smallest and the largest number in an array and prints them on the screen. The methods are tested with unit tests.

## Ostokset

Syöttää muutamia tavaroita ostoslistaan ja tulostaa listan tavaroista, tavaroiden hinnan sekä laskun kokonaissumman ruudulle. Laskun loppusumma-metodi on varmistettu yksikkötestauksella. / Enters few items to a shopping list, prints out the list, including the total price of each item and the total price of the whole invoice. The sum method of the invoice is tested with a unit test.

## MyFishApp

Kalarekisteri. Käyttäjä voi syöttää rekisteriin kalastajia ja heille saaliita. Ohjelma osaa tulostaa koko rekisterin sisällön ja suurimmat kalat pituusjärjestyksessä. / Fish register. User can input fishers and fish into the register. The program can print the whole register and biggest fishes starting with the one that weights the most.

## Kuviot

Ohjelmassa on abstrakti Shape-luokka, jossa pari abstraktia metodia. Luokasta peritään Circle- ja Rectangle-luokat ja toteutetaan niille metodit. Lopuksi luodaan muutama Cirle- ja Rectangle-luokan olio, kootaan ne listaan ja tulostetaan listan sisältö. / Program has an abstract class called Shapes and in it a few abstract methods. We inherit Circle and Rectangle class from Shape and implement methods. Last we create few objects from Circle and Rectangle and add them to a list and print that list.

## Linkkitorni

Ohjelmassa ratkaisee mikä lähetin antaa vahvimman signaalin tietyissä pisteissä. Ohjelmassa on kolme lähetintä tietyissä koordinaatiston pisteissä ja lähettimet antavat tietyn tehon, sekä vastaanottimet neljässä pisteessä. Ohjelma tulostaa ruudulle kunkin vastaanottimen suhteen vahvimman signaalin antavan lähettimen koordinaatit sekä signaalin tehon. / The program solves which transmitter gives the strongest signal in certain points. The program has three transmitters in certain coordinates, each transmitter has certain reach and receivers on four different points. The program prints on screen the strongest signal and the source of it for each receiver.